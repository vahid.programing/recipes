import {Injectable} from '@angular/core';
import {Observable, Subject} from "rxjs";
import {StateService} from "../../recipes-project/interfaces/state-service";

@Injectable({
  providedIn: 'root'
})
export class StateImplService implements StateService{

  private subject = new Subject();

  send(value: string) {
    this.subject.next({ data: value });
  }

  clearMessages() {
    this.subject.next('');
  }

  get(): Observable<any> {
    return this.subject.asObservable();
  }
}
