import {Injectable} from '@angular/core';
import {StorageService} from "../../recipes-project/interfaces/storage-service";

@Injectable({
  providedIn: 'root'
})
export class StorageImplService implements StorageService{

  constructor() {
  }

  get(key: string): any  {
    let storageItem;
    storageItem = localStorage.getItem(key);
    return storageItem  ;
  }

  addJson(key: string, value: object) {
    localStorage.setItem(key, JSON.stringify(value))
  }

  countFromJson(key: string): number {
    let itemStorage;
    itemStorage = localStorage.getItem(key);

    itemStorage = itemStorage == null ? [] : JSON.parse(itemStorage);
    return itemStorage.length;
  }



}
