import {Injectable} from '@angular/core';
import {Recipe} from "../recipes-project/interfaces/models/recipe";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {RecipesService} from "../recipes-project/interfaces/recipesService";

@Injectable({
  providedIn: 'root'
})
export class
RecipeImplService implements RecipesService{



  constructor(private http: HttpClient) { }

  getData(): Observable<Recipe[]> {
    return this.http.get<Recipe[]>("./assets/data.json")

  }

}
