import {Component, OnInit} from '@angular/core';
import {StateImplService} from "../services/util/state-impl.service";
import {StorageImplService} from "../services/util/storage-Impl.service";

@Component({
  selector: 'app-top-bar',
  templateUrl: './top-bar.component.html',
  styleUrls: ['./top-bar.component.css']
})
export class TopBarComponent implements OnInit{
  showModal = false;

  myRecipeCount = 0;

  constructor(private state: StateImplService, private storage:StorageImplService) {

    this.state.get().subscribe(res => {
      this.myRecipeCount = res.data;
    })
  }

  ngOnInit(): void {
    this.myRecipeCount = this.storage.countFromJson("myRecipes");
  }



  showMyRecipes() {
    this.showModal = true;


  }
}
