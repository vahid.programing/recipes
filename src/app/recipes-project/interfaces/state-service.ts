import {Observable} from "rxjs";

export interface StateService {
  send(value: string): void ;

  clearMessages(): void ;

  get(): Observable<any>;
}
