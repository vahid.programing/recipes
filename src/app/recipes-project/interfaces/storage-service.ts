export interface StorageService {
  get(key: string): any;

  addJson(key: string, value: object) : void ;

  countFromJson(key: string): number;


}
