import {Observable} from "rxjs";
import {Recipe} from "./models/recipe";

export interface RecipesService {
  getData(): Observable<Recipe[]>;
}
