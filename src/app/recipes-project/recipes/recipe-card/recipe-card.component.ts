import {Component, EventEmitter, Input, OnChanges, Output, SimpleChanges} from '@angular/core';
import {FormControl, FormGroup} from '@angular/forms';
import {Recipe} from "../../interfaces/models/recipe";
import {StateImplService} from "../../../services/util/state-impl.service";
import {MessageService} from "primeng/api";
import {StorageImplService} from "../../../services/util/storage-Impl.service";
import {Message} from "../../../enums/message";

@Component({
  selector: 'app-recipe-card',
  templateUrl: './recipe-card.component.html',
  styleUrls: ['./recipe-card.component.css']
})
export class RecipeCardComponent implements OnChanges {

  @Input() recipeItem: any;
  @Input() showDeleteButton: boolean = false;
  @Output() delete  =new EventEmitter<Recipe>();

  constructor(private state: StateImplService, private messageService: MessageService, private storage: StorageImplService) {

  }

  formGroup = new FormGroup({
    img: new FormControl(''),
    title: new FormControl(''),
    description: new FormControl(''),
  })

  ngOnChanges(changes: SimpleChanges): void {
    this.formGroup.patchValue(this.recipeItem);
  }


  saveOnMyRecipe() {
    let storageRecipes = this.storage.get("myRecipes");
    storageRecipes = storageRecipes == null ? [] : JSON.parse(storageRecipes);
    storageRecipes.push(this.recipeItem);
    this.storage.addJson("myRecipes", storageRecipes);
    this.state.send(storageRecipes.length);
    this.messageService.add({key: 'mainToast', severity: Message.success, summary: 'Success', detail: 'Successfully'});
  }

  deleteFromMyRecipe() {
    this.delete.emit(this.recipeItem);
  }


}
