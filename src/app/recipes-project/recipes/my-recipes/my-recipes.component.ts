import {Component, OnInit} from '@angular/core';
import * as _ from "lodash";
import {StorageImplService} from "../../../services/util/storage-Impl.service";
import {StateImplService} from "../../../services/util/state-impl.service";
import {MessageService} from "primeng/api";
import {Recipe} from "../../interfaces/models/recipe";
import {Message} from "../../../enums/message";

@Component({
  selector: 'app-my-recipes',
  templateUrl: './my-recipes.component.html',
  styleUrls: ['./my-recipes.component.css']
})
export class MyRecipesComponent implements OnInit {
  myRecipeList: Recipe[] = [];


  constructor(private state:StateImplService, private storage:StorageImplService, private messageService:MessageService) {
  }

  ngOnInit(): void {
    this.loadMyRecipe();

  }

  loadMyRecipe() {
    let myRecipes;
    myRecipes = localStorage.getItem("myRecipes");
    this.myRecipeList = myRecipes == null ? [] : JSON.parse(myRecipes);
  }

  delete(recipeItem : Recipe) {
    console.log(event);
    let storageRecipes = this.storage.get("myRecipes");
    storageRecipes = storageRecipes == null ? [] : JSON.parse(storageRecipes);
    let index = _.findIndex(storageRecipes, {id: recipeItem.id})
    storageRecipes.splice(index, 1);
    this.myRecipeList.splice(index, 1);
    this.storage.addJson("myRecipes", storageRecipes);
    this.state.send(storageRecipes.length);
    this.messageService.add({key: 'mainToast', severity: Message.info, summary: 'Delete', detail: 'Deleted!'});
  }
}
