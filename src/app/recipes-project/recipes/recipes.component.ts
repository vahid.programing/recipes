import {Component, OnInit} from '@angular/core';
import {RecipeImplService} from "../../services/recipe-impl.service";
import {Recipe} from "../interfaces/models/recipe";
import {FormControl} from "@angular/forms";


@Component({
  selector: 'app-recipes',
  templateUrl: './recipes.component.html',
  styleUrls: ['./recipes.component.css']
})
export class RecipesComponent implements OnInit {


  recipeList: Recipe[] = [];
  searchControl = new FormControl();

  constructor(private recipeService: RecipeImplService) {
  }


  getData() {
    this.recipeService.getData().subscribe((response:Recipe[]) => {
      this.recipeList = response;
    })
  }

  filterRecipe(recipe: any, searchVerb: any) {
    if (recipe.description.toLowerCase().trim().indexOf(searchVerb) != -1 || recipe.title.toLowerCase().trim().indexOf(searchVerb) != -1)
      return recipe
  }

  ngOnInit(): void {

    this.searchControl.valueChanges.subscribe(search => {
      if(search==''){
        this.getData();
      }
     let recipes = this.recipeList.filter(recipe => this.filterRecipe(recipe, search));
      this.recipeList= recipes;
    });

    this.getData();
  }


}
