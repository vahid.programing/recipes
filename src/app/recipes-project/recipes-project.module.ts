import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {RecipesComponent} from "./recipes/recipes.component";
import {RecipeCardComponent} from "./recipes/recipe-card/recipe-card.component";
import {MyRecipesComponent} from "./recipes/my-recipes/my-recipes.component";
import {ButtonModule} from "primeng/button";
import {CardModule} from "primeng/card";
import {AppRoutingModule} from "../app-routing.module";
import {ReactiveFormsModule} from "@angular/forms";
import {HttpClientModule} from "@angular/common/http";
import {LoadingBarHttpClientModule} from "@ngx-loading-bar/http-client";
import {LoadingBarModule} from "@ngx-loading-bar/core";
import {TagModule} from "primeng/tag";
import {SidebarModule} from "primeng/sidebar";
import {DialogModule} from "primeng/dialog";
import {NoopAnimationsModule} from "@angular/platform-browser/animations";
import {ToastModule} from "primeng/toast";
import {MessageModule} from "primeng/message";
import {MessagesModule} from "primeng/messages";
import {InputTextModule} from "primeng/inputtext";
import {SummaryPipe} from "../pipes/summary.pipe";
import {HighlighterDirective} from "../directives/highlighter.directive";


@NgModule({
  declarations: [
    SummaryPipe,

    HighlighterDirective,
    RecipesComponent,
    RecipeCardComponent,
    MyRecipesComponent,
  ],
  exports: [
    MyRecipesComponent,
    RecipesComponent
  ],
  imports: [
    CommonModule,
    AppRoutingModule, ReactiveFormsModule, HttpClientModule,// for HttpClient use:
    LoadingBarHttpClientModule, LoadingBarModule, ButtonModule, CardModule, TagModule, SidebarModule, DialogModule,
    NoopAnimationsModule, ToastModule, MessageModule,
    MessagesModule, InputTextModule,

    CardModule
  ]
})
export class RecipesProjectModule { }
