import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'summary'
})
export class SummaryPipe implements PipeTransform {

  transform(value: any, ...args: unknown[]): unknown {

    return value.substr(0,150)+"....";
    // return null;
  }

}
