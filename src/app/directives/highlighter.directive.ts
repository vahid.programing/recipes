import {Directive, ElementRef, HostListener} from '@angular/core';

@Directive({
  selector: '[Highlighter]'
})
export class HighlighterDirective {
el
  constructor(private elementRef: ElementRef) {
  this.el = elementRef;

  }
  @HostListener('mouseenter') onMouseEnter() {
    this.highlight('yellow');
  }

  @HostListener('mouseleave') onMouseLeave() {
    this.highlight('');
  }

  private highlight(color: string) {
    this.el.nativeElement.style.backgroundColor = color;
  }

}
