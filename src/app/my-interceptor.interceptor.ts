import {Injectable} from '@angular/core';
import {
  HttpErrorResponse,
  HttpEvent,
  HttpHandler,
  HttpInterceptor,
  HttpRequest,
  HttpResponse
} from '@angular/common/http';
import {catchError, map, Observable, throwError, timeout} from 'rxjs';

@Injectable()
export class MyInterceptorInterceptor implements HttpInterceptor {

  constructor() {
  }

  intercept(request: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
    return next.handle(request).pipe(
      timeout(300000),
      map((event: HttpEvent<any>) => {
          if (event instanceof HttpResponse) {
            if (event.status == 200) {
              console.log("statusCode 200")
            }
          }

        }
      ),
      catchError((error: HttpErrorResponse) => {
        switch (error.status) {
          case 400 : {
            console.log("statusCode " + error.status);
            break;
          }
          case 401 : {
            console.log("statusCode " + error.status);
            break;
          }
          case 403 : {
            console.log("statusCode " + error.status);
            break;
          }
          case 404 : {
            console.log("statusCode " + error.status);
            break;
          }
          case 500 : {
            console.log("statusCode " + error.status);
            break;
          }
          case 502 : {
            console.log("statusCode " + error.status);
            break;
          }

          case 302 : {
            console.log("statusCode " + error.status);
            break;
          }

          default : {

          }
        }
        return throwError(error);
      }));
  }
}

