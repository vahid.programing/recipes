import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppComponent} from './app.component';
import {LoadingBarModule} from '@ngx-loading-bar/core';
import {TopBarComponent} from './top-bar/top-bar.component';
import {TagModule} from "primeng/tag";
import {DialogModule} from "primeng/dialog";
import {ToastModule} from 'primeng/toast';
import {MessageService} from "primeng/api";
import {RecipesProjectModule} from "./recipes-project/recipes-project.module";
import {RouterOutlet} from "@angular/router";

@NgModule({
  declarations: [
    AppComponent,


    TopBarComponent,


  ],
  imports: [
    BrowserModule,


    RecipesProjectModule,
    DialogModule,
    TagModule,
    RouterOutlet,
    ToastModule,
    LoadingBarModule


  ],
  providers: [MessageService],
  bootstrap: [AppComponent],

  exports: [

  ]
})
export class AppModule {
}
